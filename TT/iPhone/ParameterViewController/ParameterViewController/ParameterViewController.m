//
//  ParameterViewController.m
//  TT
//
//  Created by Becario2 on 08/11/17.
//  Copyright © 2017 Mike. All rights reserved.
//

#import "ParameterViewController.h"

@interface ParameterViewController ()

@end

@implementation ParameterViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self=[super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];//this returns a viewController
    
    if(self)
    {
        //Set frame
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height)];
        
        //set labels
        _weightLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x + 30,self.navigationController.navigationBar.frame.size.height +90,200, 30)];
        [_weightLabel setBackgroundColor:[UIColor clearColor]];
        [_weightLabel setText: @"Peso (KG)"];
        [_weightLabel setTextColor:[UIColor blackColor]];
        [_weightLabel setTextAlignment:NSTextAlignmentLeft];
        [_weightLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
        [self.view addSubview:_weightLabel];
        
        _heightLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x + 30,_weightLabel.frame.origin.y + 60,200, 30)];
        [_heightLabel setBackgroundColor:[UIColor clearColor]];
        [_heightLabel setText: @"Talla (cm)"];
        [_heightLabel setTextColor:[UIColor blackColor]];
        [_heightLabel setTextAlignment:NSTextAlignmentLeft];
        [_heightLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
        [self.view addSubview:_heightLabel];
        
        _cardiacFrecuencyLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x + 30,_heightLabel.frame.origin.y + 60,200, 30)];
        [_cardiacFrecuencyLabel setBackgroundColor:[UIColor clearColor]];
        [_cardiacFrecuencyLabel setText: @"FC (ipm)"];
        [_cardiacFrecuencyLabel setTextColor:[UIColor blackColor]];
        [_cardiacFrecuencyLabel setTextAlignment:NSTextAlignmentLeft];
        [_cardiacFrecuencyLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
        [self.view addSubview:_cardiacFrecuencyLabel];
        
        _ageLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x + 30,_cardiacFrecuencyLabel.frame.origin.y + 60,200, 30)];
        [_ageLabel setBackgroundColor:[UIColor clearColor]];
        [_ageLabel setText: @"Edad (años)"];
        [_ageLabel setTextColor:[UIColor blackColor]];
        [_ageLabel setTextAlignment:NSTextAlignmentLeft];
        [_ageLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
        [self.view addSubview:_ageLabel];
        
        _sistolicPressureLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x + 30,_ageLabel.frame.origin.y + 60,200, 30)];
        [_sistolicPressureLabel setBackgroundColor:[UIColor clearColor]];
        [_sistolicPressureLabel setText: @"Presión sistólica (mmHg)"];
        [_sistolicPressureLabel setTextColor:[UIColor blackColor]];
        [_sistolicPressureLabel setTextAlignment:NSTextAlignmentLeft];
        [_sistolicPressureLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
        [self.view addSubview:_sistolicPressureLabel];
        
        _diastolicPressureLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.origin.x + 30,_sistolicPressureLabel.frame.origin.y + 60,200, 30)];
        [_diastolicPressureLabel setBackgroundColor:[UIColor clearColor]];
        [_diastolicPressureLabel setText: @"Presión diastólica (mmHg)"];
        [_diastolicPressureLabel setTextColor:[UIColor blackColor]];
        [_diastolicPressureLabel setTextAlignment:NSTextAlignmentLeft];
        [_diastolicPressureLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
        [self.view addSubview:_diastolicPressureLabel];
        
        //set TextFields
        _weightTextField = [[UITextField alloc] initWithFrame:CGRectMake(_weightLabel.frame.size.width + 20, self.navigationController.navigationBar.frame.size.height +90, 90, 40)];
        _weightTextField.borderStyle = UITextBorderStyleRoundedRect;
        _weightTextField.font = [UIFont systemFontOfSize:15];
        _weightTextField.placeholder = @"ej: 80.25";
        _weightTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _weightTextField.keyboardType = UIKeyboardTypeDefault;
        _weightTextField.returnKeyType = UIReturnKeyDone;
        _weightTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _weightTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _weightTextField.delegate = self;
        [self.view addSubview:_weightTextField];
        
        _heightTextField = [[UITextField alloc] initWithFrame:CGRectMake(_heightLabel.frame.size.width + 20, _weightTextField.frame.origin.y + 60, 90, 40)];
        _heightTextField.borderStyle = UITextBorderStyleRoundedRect;
        _heightTextField.font = [UIFont systemFontOfSize:15];
        _heightTextField.placeholder = @"ej: 128.5";
        _heightTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _heightTextField.keyboardType = UIKeyboardTypeDefault;
        _heightTextField.returnKeyType = UIReturnKeyDone;
        _heightTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _heightTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _heightTextField.delegate = self;
        [self.view addSubview:_heightTextField];
        
        _cardiacFrecuencyTextField = [[UITextField alloc] initWithFrame:CGRectMake(_cardiacFrecuencyLabel.frame.size.width + 20, _heightTextField.frame.origin.y + 60, 90, 40)];
        _cardiacFrecuencyTextField.borderStyle = UITextBorderStyleRoundedRect;
        _cardiacFrecuencyTextField.font = [UIFont systemFontOfSize:15];
        _cardiacFrecuencyTextField.placeholder = @"ej: 60";
        _cardiacFrecuencyTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _cardiacFrecuencyTextField.keyboardType = UIKeyboardTypeDefault;
        _cardiacFrecuencyTextField.returnKeyType = UIReturnKeyDone;
        _cardiacFrecuencyTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _cardiacFrecuencyTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _cardiacFrecuencyTextField.delegate = self;
        [self.view addSubview:_cardiacFrecuencyTextField];
        
        _ageTextField = [[UITextField alloc] initWithFrame:CGRectMake(_ageLabel.frame.size.width + 20, _cardiacFrecuencyTextField.frame.origin.y + 60, 90, 40)];
        _ageTextField.borderStyle = UITextBorderStyleRoundedRect;
        _ageTextField.font = [UIFont systemFontOfSize:15];
        _ageTextField.placeholder = @"ej: 30";
        _ageTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _ageTextField.keyboardType = UIKeyboardTypeDefault;
        _ageTextField.returnKeyType = UIReturnKeyDone;
        _ageTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _ageTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _ageTextField.delegate = self;
        [self.view addSubview:_ageTextField];
        
        _sistolicPressureTextField = [[UITextField alloc] initWithFrame:CGRectMake(_weightLabel.frame.size.width + 20, _ageTextField.frame.origin.y + 60, 90, 40)];
        _sistolicPressureTextField.borderStyle = UITextBorderStyleRoundedRect;
        _sistolicPressureTextField.font = [UIFont systemFontOfSize:15];
        _sistolicPressureTextField.placeholder = @"ej: 120";
        _sistolicPressureTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _sistolicPressureTextField.keyboardType = UIKeyboardTypeDefault;
        _sistolicPressureTextField.returnKeyType = UIReturnKeyDone;
        _sistolicPressureTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _sistolicPressureTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _sistolicPressureTextField.delegate = self;
        [self.view addSubview:_sistolicPressureTextField];
        
        _diastolicPressureTextField = [[UITextField alloc] initWithFrame:CGRectMake(_weightLabel.frame.size.width + 20, _sistolicPressureTextField.frame.origin.y + 60, 90, 40)];
        _diastolicPressureTextField.borderStyle = UITextBorderStyleRoundedRect;
        _diastolicPressureTextField.font = [UIFont systemFontOfSize:15];
        _diastolicPressureTextField.placeholder = @"ej: 80";
        _diastolicPressureTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _diastolicPressureTextField.keyboardType = UIKeyboardTypeDefault;
        _diastolicPressureTextField.returnKeyType = UIReturnKeyDone;
        _diastolicPressureTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _diastolicPressureTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _diastolicPressureTextField.delegate = self;
        [self.view addSubview:_diastolicPressureTextField];
        
        
        _cameraButton = [UIButton buttonWithType: UIButtonTypeCustom];
        [_cameraButton setFrame:CGRectMake(self.view.frame.size.width/2 - 45, self.view.frame.size.height - 150, 70, 50)];
        [_cameraButton setImage:[UIImage imageNamed:@"cameraIcon"] forState:UIControlStateNormal];
        [_cameraButton addTarget:self action:@selector(cameraButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_cameraButton];
    }
    
    return self;
}

-(void)cameraButtonPressed
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"¿Éstas seguro de que quieres continuar?" message:@"No podrás modificar los valores ingresados" preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    
                                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                    NSLog(@"CameraButton Pressed");
                                    CameraViewController *_cameraViewController = [[CameraViewController alloc] initWithNibName:@"CameraViewController" bundle:nil];
                                    [self.navigationController pushViewController:_cameraViewController animated:YES];
                                    
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alertController addAction:yesButton];
    [alertController addAction:noButton];
    
    
    [self presentViewController:alertController animated:YES completion:nil];
    
  
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"ViewDidAppear");
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"ViewWillAppear");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
