//
//  ParameterViewController.h
//  TT
//
//  Created by Becario2 on 08/11/17.
//  Copyright © 2017 Mike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CameraViewController.h"

@interface ParameterViewController : UIViewController <UITextFieldDelegate>
{
    UILabel *_weightLabel;
    UILabel *_ageLabel;
    UILabel *_heightLabel;
    UILabel *_cardiacFrecuencyLabel;
    UILabel *_sistolicPressureLabel;
    UILabel *_diastolicPressureLabel;
    UILabel *_genderLabel;
    
    UITextField *_weightTextField;
    UITextField *_ageTextField;
    UITextField *_heightTextField;
    UITextField *_cardiacFrecuencyTextField;
    UITextField *_sistolicPressureTextField;
    UITextField *_diastolicPressureTextField;
    UITextField *_genderTextField;
    
    float _weightValue;
    float _heightValue;
    NSInteger _ageValue;
    NSInteger _sistolicPressureValue;
    NSInteger _diastolicPressureValue;

    UIButton *_cameraButton;
}

-(void) cameraButtonPressed;

@end
