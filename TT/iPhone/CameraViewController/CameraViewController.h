//
//  CameraViewController.h
//  TT
//
//  Created by Becario2 on 09/11/17.
//  Copyright © 2017 Mike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraViewController : UIViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@end
