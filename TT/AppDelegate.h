//
//  AppDelegate.h
//  TT
//
//  Created by Becario2 on 08/11/17.
//  Copyright © 2017 Mike. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParameterViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

